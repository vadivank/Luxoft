#include <iostream>
#include <bitset>

// ЦИКЛОМ конверсия опеределенных битов без использования встроенных функций
void binConvert1(auto &value, int &pos, int &num){
    for (int count = 0; count < num; ++count) value = value ^ (1 << count + pos);    
}

// МАСКОЙ конверсия опеределенных битов без использования встроенных функций
void binConvert2(auto &value, int &pos, int &num) {
    int mask = (1 << num) - 1;    
    value ^= (mask << pos);
}


int main(){
    int pos = 3; // сколько позиций нужно пропустить, чтобы начать работать с битами
    int num = 3; // сколько битов конвертировать
    //unsigned char value = 0b1011'1011;
    long long value = 187;
    
    std::cout << " BEFORE convert: value -> " << value << " and binary expression -> " << std::bitset<8 * sizeof(value)>(value) << "\n";
    binConvert1(value, pos, num);
    std::cout << " AFTER  convert: value -> " << value << " and binary expression -> " << std::bitset<8 * sizeof(value)>(value) << "\n";

    std::cout << "------------------\n";

    std::cout << " BEFORE convert: value -> " << value << " and binary expression -> " << std::bitset<8 * sizeof(value)>(value) << "\n";
    binConvert2(value, pos, num);
    std::cout << " AFTER  convert: value -> " << value << " and binary expression -> " << std::bitset<8 * sizeof(value)>(value) << "\n";

    return 0;
}
