#include <iostream>
#include <cstring>

void getCount(char* string, int len){
    int repeat;
    int count;
    for (int i = 0; i < len; ++i){
        repeat = 1;
        count = 0;
        for (int p = 0; p < i; ++p){
            if (string[i] == string[p]){
                count = 1;
                break;
            }
        }

        if (!count){
            for (int j = i+1; j < len; ++j){
                if (string[i] == string[j]){
                    ++repeat;
                }
            }
            std::cout << " \'"<< string [i] << "\' ---> " << repeat << "\n";
        }
    }
}


int main(){
    char str[] = "Abra cadabra sim salabim";
    getCount(str, strlen(str));
    return 0;
}
