#include <iostream>
#include <cmath>

static const double PI = 3.14159265;

class Circ {
    //дефиниция класса
private:
    double r_;

public:
    //Circ();                   //конструктор по-умолчанию
    Circ(double radius = 1);    //конструктор параметра (явно объявить, чтобы избавиться от пустого констр)
    Circ(const Circ& circ);     //конструктор копирования
    ~Circ();
    
    Circ& operator=(const Circ& c); // cannot be non-member

    double getR() const;
    double getS() const;
    double getP() const;
    void setR(double val);
};

//Circ::Circ() : Circ(1) {}
Circ::Circ(double rad) : r_(rad) { std::cout << " +++ ctor: " << this << "\n"; }
Circ::Circ(const Circ& circus) : Circ(circus.r_) { this->r_ = circus.r_; }
Circ::~Circ() { std::cout << " --- dtor: " << this << std::endl; }

std::ostream& operator<<(std::ostream& outStream, const Circ& circ){
    outStream 
        << "R = " << circ.getR() 
        << ", S = " << circ.getS() 
        << ", P = " << circ.getP() << std::endl;
    return outStream;
}

std::istream& operator>>(std::istream& inStream, Circ& circ){
    int radius;
    inStream >> radius;
    circ.setR(radius);
    return inStream;
}

Circ operator+(const Circ& c1, const Circ& c2){
    return (c1.getR() + c2.getR());
}
// Circ operator+(const Circ& c1, int val){         // как оно работает с c9?
//     return (c1.getR() + val);
// }

Circ operator-(const Circ& c1, const Circ& c2){
    return (c1.getR() - c2.getR());
}
// Circ operator-(const Circ& c1, int val){         // как оно работает с c8?
//     return (c1.getR() - val);
// }

Circ& Circ::operator=(const Circ& c){
    setR(c.getR());
    return *this;
}

double Circ::getR() const { return r_; }
double Circ::getS() const { return PI * r_ * r_; }
double Circ::getP() const { return PI * r_ * 2; }
void Circ::setR(double val) { r_ = val; }



int main(){

    Circ c1; 
    c1.setR(10);
    std::cout << "c1.setR(10)\nc1: " << c1 << std::endl;

    Circ c2(50);
    std::cout << "Circ c2(50)\nc2: " << c2 << std::endl;

    Circ c3 = 100;
    std::cout << "Circ c3 = 100\nc3: " << c3 << std::endl;

    Circ c4 = c3;
    std::cout << "Circ c4 = c3\nc4: " << c4 << std::endl;

    Circ c5(c4);
    std::cout << "Circ c5(c4)\nc5:" << c5 << std::endl;

    Circ c6 = c2 + c5;
    std::cout << "Circ c6 = c2 + c5\nc6: " << c6 << std::endl;

    Circ c7 = c3 - c2;
    std::cout << "Circ c7 = c3 - c2\nc7: " << c7 << std::endl;

    Circ c8 = c3 - 10;
    std::cout << "Circ c8 = c3 - 10\nc8: " << c8 << std::endl;

    Circ c9 = c3 + 30;
    std::cout << "Circ c9 = c3 + 30\nc9: " << c9 << std::endl;

    return 0;
}
